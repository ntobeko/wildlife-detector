package za.co.nto;

import org.apache.commons.lang.StringUtils;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**                   
 *   Basic wildlife object detector that detects objects in images
 *   from an aerial vantage point.
 *
 *   Copyright (C) 2018 Ntobeko Wakaba
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *   Author's contact information:
 *   <a href="mailto:ntobeko@gmail.com">ntobeko@gmail.com</a>
 */
public class BasicDetector
{
    /** General logger for the class */
    private static final Logger LOGGER =
                LoggerFactory.getLogger(BasicDetector.class);

    /** This is the fully qualified image file name that is currently being processed */
    private String currentFile;
    
    /** The result of the rendering is written to this member variable */
    private BufferedImage currentImage = null;

    /** Icon object in which rendered image is placed. */
    private ImageIcon imgCanvas;

    /** Original unprocessed image on which detection is to be carried out */
    private Mat originalMat;

    /**
     * The object detector is constructed by supplying a fully qualified
     * image file name and image canvas on which rendering is to be done.
     *
     * @param currentFile   This is the fully qualified file name
     *                      of the image to process. If the image
     *                      file has not changed from the previous
     *                      render, it is not loaded from disk.
     * @param imgCanvas     This is an icon object that the rendered image
     *                      is contained in.
     */
    public BasicDetector(String currentFile, ImageIcon imgCanvas)
    {
        this.currentFile = currentFile;
        this.imgCanvas = imgCanvas;
    }

    /**
     * Detect and render bounded boxes based on the set detection parameters.
     *
     * @param imageFile             This is the fully qualified file name
     *                              of the image to process. If the image
     *                              file has not changed from the previous
     *                              render, it is not loaded from disk.
     * @param thresholdValue        This is the threshold value to apply.
     * @param showOriginal          This is a flag that indicates whether
     *                              the identified contour bounding boxes
     *                              will be drawn on the original image or
     *                              on the threshold image.
     * @param invertThresholdImage  This is a flag indicating whether an
     *                              inverted binary threshold is applied or not.
     */
    public void render(String imageFile, int thresholdValue,
                       boolean showOriginal,
                       boolean invertThresholdImage)
    {
        // Ensure nothing happens if no file is chosen.
        if (StringUtils.isEmpty(imageFile))
        {
            return;
        }

        // Ensure a new image is loaded if the file name changes.
        if (!imageFile.equals(currentFile))
        {
            if (!new File(imageFile).exists())
            {
                return;
            }
            this.currentFile = imageFile;
            this.originalMat = Imgcodecs.imread(imageFile);
            this.currentImage = new BufferedImage(
                                        originalMat.width(),
                                        originalMat.height(),
                                        BufferedImage.TYPE_3BYTE_BGR);
            this.imgCanvas.setImage(currentImage);
        }

        //Method converts a Mat to a Buffered Image
        final byte[] targetPixels = ((DataBufferByte) this.currentImage
                                        .getRaster().getDataBuffer()).getData();
        byte[] targetBytes = new byte[targetPixels.length];

        Mat destinationImg = doThresholdAndDetect(this.originalMat, thresholdValue,
                                showOriginal, invertThresholdImage);

        destinationImg.get(0, 0, targetBytes);
        System.arraycopy(targetBytes, 0, targetPixels,
                0, targetBytes.length);
    }

    /**
     * This method applies a median image blur of size 5x5 pixels, then
     * applies the chosen binary threshold based on the parameters
     * <code>threshold</code> and <code>invertThresholdImage</code>.
     *
     * @param unprocessedImg        This is the original unprocessed image.
     * @param threshold             This is the current threshold value to
     *                              apply to the image.
     * @param showOriginal          This is a flag that indicates whether
     *                              the identified contour bounding boxes
     *                              will be drawn on the original image or
     *                              on the threshold image.
     * @param invertThresholdImage  This is a flag indicating whether an
     *                              inverted binary threshold is applied or not.
     *
     * @return                      An image with identified objects is returned
     *                              to the caller.
     */
    private Mat doThresholdAndDetect(Mat unprocessedImg, int threshold,
                                     boolean showOriginal,
                                     boolean invertThresholdImage)
    {
        // First increase contrast using CLAHE algo
        Mat blurredImg = new Mat();
        Imgproc.blur(unprocessedImg, blurredImg, new Size(5, 5));

        // init
        Mat hsvImg = new Mat();
        List<Mat> hsvPlanes = new ArrayList<>();
        Mat thresholdImg = new Mat();

        // Convert the blurred image to HSV
        hsvImg.create(blurredImg.size(), CvType.CV_8U);
        Imgproc.cvtColor(blurredImg, hsvImg, Imgproc.COLOR_BGR2HSV);

        // Split HSV planes
        Core.split(hsvImg, hsvPlanes);

        // threshold the image with the selected threshold parameter;
        // applicable to the 'hue' plane.
        Imgproc.threshold(hsvPlanes.get(0), thresholdImg, threshold, 255,
                    invertThresholdImage ?
                            Imgproc.THRESH_BINARY_INV :
                            Imgproc.THRESH_BINARY);

        // Detect objects by enhancing edges and finding contours.
        return getContours(thresholdImg, hsvPlanes, unprocessedImg,
                    showOriginal);
    }

    /**
     * This method finds contours in the given image and draws
     * bounding boxes around identified contours.
     *
     * @param thresholdImg  This is the image resulting from a
     *                      binary threshold process being applied
     *                      on the original image.
     * @param hsvPlanes     This is a list of matrices representing
     *                      the 'hue', 'saturation', and 'value'
     *                      components of the original image.
     * @param originalMat   This is a matrix representing the original
     *                      image.
     * @param showOriginal  This is a flag that indicates whether the
     *                      identified contour boxes must be rendered
     *                      on the original image or on the threshold
     *                      processed image.
     *
     * @return              An image with the rendered bounding boxes
     *                      is returned to the caller.
     */
    private Mat getContours(Mat thresholdImg, List<Mat> hsvPlanes,
                            Mat originalMat, boolean showOriginal)
    {
        // Enhance edges in image by dilating to fill gaps, and eroding
        // to smooth edges; repeat the process 3 times.
        for (int j = 0; j < 3; j++)
        {
            Imgproc.dilate(thresholdImg, thresholdImg, new Mat(),
                        new Point(-1, -1), 1);
            Imgproc.erode(thresholdImg, thresholdImg, new Mat(),
                        new Point(-1, -1), 3);
        }

        // Now that the threshold image has edges enhanced, find
        // all the contours.
        List<MatOfPoint> contours = new ArrayList<>();
        Imgproc.findContours(thresholdImg, contours, new Mat(),
                    Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_NONE);

        // Filter out unwanted contours.
        contours = filterContours(contours, thresholdImg);

        LOGGER.info("contours: " + contours.size());

        Mat bgr = new Mat();
        if (showOriginal)
        {
            originalMat.copyTo(bgr);
        }
        else
        {
            // Draw the filtered contours on threshold image.
            Imgproc.drawContours(thresholdImg, contours, -1,
                    new Scalar(0, 0, 255), 3);
            hsvPlanes.set(0, thresholdImg);
            Mat hsvMerged = new Mat();
            Core.merge(hsvPlanes, hsvMerged);
            Imgproc.cvtColor(hsvMerged, bgr, Imgproc.COLOR_HSV2BGR);
        }

        // Draw bounding boxes around identified contours.
        if (contours.size() < 100)
        {
            Rect rect;
            for (MatOfPoint matOfPoint : contours)
            {
                rect = Imgproc.boundingRect(matOfPoint);
                Imgproc.rectangle(bgr, new Point(rect.x, rect.y),
                            new Point(rect.x + rect.width,
                                    rect.y + rect.height),
                            new Scalar(0, 0, 0), 3);
            }
        }
        return bgr;
    }

    /**
     * This method filters a list of <code>MatOfPoints</code> that
     * represent contours and only returns those that have an area
     * of between 2% and 80% of the total image size.
     *
     * @param contours  This is the list of <code>MatOfPoint</code>
     *                  contours that is to be filtered.
     *
     * @param targetMat This is the image from which the given
     *                  contours are derived.
     *
     * @return          A reduced list of contours is returned to
     *                  the caller.
     */
    private List<MatOfPoint> filterContours(List<MatOfPoint> contours,
                                            Mat targetMat)
    {
        List<MatOfPoint> contoursToKeep = new ArrayList<>();

        // Minimum contour area to include is 2% of the original image.
        double areaMin = targetMat.width() * targetMat.height() * .02;

        // Maximum contour area to include is 80% of the original image.
        double areaMax = targetMat.width() * targetMat.height() * .8;

        contours.forEach(contour ->
        {
            double area = Imgproc.contourArea(contour);
            if (area > areaMin && area < areaMax)
            {
                contoursToKeep.add(contour);
            }
        });

        return contoursToKeep;
    }
}