package za.co.nto;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.opencv.core.Core;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**                   
 *   Basic wildlife object detector that detects objects in images
 *   from an aerial vantage point.
 *
 *   Copyright (C) 2018 Ntobeko Wakaba
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *   Author's contact information:
 *   <a href="mailto:ntobeko@gmail.com">ntobeko@gmail.com</a>
 */
public class BasicDetectorGUI
{
    /** General logger for the class */
    private static final Logger LOGGER =
            LoggerFactory.getLogger(BasicDetectorGUI.class);

    /**
     * Miscellaneous application GUI constants
     */
    private static final String APP_TITLE = "Wildlife Detector";
    private static final int DEFAULT_APP_WIDTH = 800;
    private static final int DEFAULT_APP_HEIGHT = 500;
    private static final String[] IMAGE_EXTENSIONS =
                                    new String[] { "jpg", "png", "bmp" };

    /*
     * Object detector run parameters
     */

    /**
     * Running threshold value to apply binary threshold on 'hue'
     * plane. Minimum value is <code>0</code> and maximum value
     * is <code>255</code>.
     */
    private int thresholdValue = 0;

    /**
     * Flag indicator that determines whether the bounding rectangles
     * highlighting detected objects is overlaid on the original image
     * or on output of the processed image.
     */
    private boolean showOriginalImage = false;

    /**
     * This is the current file selected by the user on the GUI.
     */
    private String currentFile = null;

    /**
     * This is the current directory selected by the user on the GUI.
     */
    private String currentDir = new File("").getAbsolutePath();

    /**
     * Based on the image characteristics, the user may elect to use
     * an inverted binary image threshold to make the background of
     * the image homogeneous.
     */
    private boolean invertThresholdImage;

    /*
     * Various GUI elements to show render results
     */
    private JLabel lblThresholdValue = new JLabel("Threshold: 000");
    private ImageIcon imgCanvas = new ImageIcon();
    private JLabel imagePanel = new JLabel();

    /**
     * The object detector class that encapsulates the object detection logic.
     */
    private BasicDetector detector;


    /**
     * This default constructor sets up the object detector GUI
     * and creates an initial object detector.
     */
    public BasicDetectorGUI()
    {
        // Load OpenCV native library
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        // Setup the basic user interface
        initGUI();

        // Create the initial object detector
        detector = new BasicDetector(
                    currentDir + File.separator + currentFile,
                        imgCanvas);
    }

    /**
     * This method handles the object detector GUI construction.
     */
    private void initGUI()
    {
        // Create the application frame in a maximised state.
        JFrame appFrame = new JFrame(APP_TITLE);
        appFrame.setSize(DEFAULT_APP_WIDTH, DEFAULT_APP_HEIGHT);

        // Default state of the application window is maximised.
        appFrame.setExtendedState(Frame.MAXIMIZED_BOTH);
        appFrame.setLayout(new BorderLayout());

        // Exit application when window is closed.
        appFrame.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                super.windowClosing(e);
                System.exit(0);
            }
        });

        // Setup the top panel containing the image file
        // location and file selection.
        setupTopControlPanel(appFrame);

        // Setup the rendering canvas showing results of
        // the object detection.
        setupCanvas(appFrame);

        // Setup the bottom panel with components for
        // threshold control and application exit.
        setupBottomControlPanel(appFrame);

        // Show the GUI.
        appFrame.setVisible(true);
    }

    /**
     * This method sets up the bottom panel with components for
     * threshold control and application exit.
     *
     * @param appFrame  This is the application frame to which
     *                  the bottom panel is to be added.
     */
    private void setupBottomControlPanel(JFrame appFrame)
    {
        // Setup grid layout for the panel.
        JPanel bottomPanel = new JPanel();
        bottomPanel.setLayout(new GridLayout(2, 1));

        // Align top part left and bottom part right.
        JPanel topPart = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel bottomPart = new JPanel(new FlowLayout(FlowLayout.RIGHT));

        bottomPanel.add(topPart);
        bottomPanel.add(bottomPart);

        // Add a threshold slider to the panel.
        JSlider slider = new JSlider(0, 255, thresholdValue);
        lblThresholdValue.setLabelFor(slider);
        topPart.add(lblThresholdValue);
        topPart.add(slider);

        // Add a change listener to the slider; when the slider is moved,
        // the object detection is executed and the results rendered.
        slider.addChangeListener(event ->
        {
            NumberFormat nf = NumberFormat.getInstance();
            nf.setMinimumIntegerDigits(3);
            nf.setMaximumIntegerDigits(3);
            thresholdValue = slider.getValue();
            lblThresholdValue.setText("Threshold: " +
                    nf.format(thresholdValue));
            renderDetection();
        });

        // Add checkbox to allow user to invert the binary threshold
        // computation. Every time this parameter is changed, detection
        // is rerun and rendered.
        JCheckBox cbInvert = new JCheckBox();
        topPart.add(new JLabel("Invert Threshold"));
        topPart.add(cbInvert);
        cbInvert.addChangeListener(event ->
        {
            invertThresholdImage = cbInvert.isSelected();
            renderDetection();
        });


        // Add exit button and associated listener.
        JButton btnExit = new JButton("Exit");
        bottomPart.add(btnExit);
        btnExit.addActionListener(event -> System.exit(0));

        appFrame.add(bottomPanel, BorderLayout.SOUTH);
    }

    /**
     * This method sets up the rendering canvas that shows
     * object detection results.
     *
     * @param appFrame  This is the application frame to which
     *                  the rendering canvas is to be added.
     */
    private void setupCanvas(JFrame appFrame)
    {
        imagePanel.setIcon(imgCanvas);

        // Ensure scrolling is possible if the image is larger
        // than the view area.
        JScrollPane scrollPane = new JScrollPane(imagePanel,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        appFrame.add(scrollPane, BorderLayout.CENTER);
    }

    /**
     * Setup the top panel containing the image file
     * location and file selection.
     *
     * @param appFrame  This is the application frame to which
     *                  the control components are to be added.
     */
    private void setupTopControlPanel(JFrame appFrame)
    {
        JPanel topPanel = new JPanel();

        // Checkbox to indicate original image background
        // must be used or not.
        JLabel lblUseOriginalImage = new JLabel(
                                    "Show original image");
        JCheckBox cbUseOriginalImage = new JCheckBox();

        // Add listener to detect and render after option change.
        cbUseOriginalImage.addChangeListener(event ->
        {
            showOriginalImage = cbUseOriginalImage.isSelected();
            renderDetection();
        });

        // File chooser
        JComboBox comboFileChooser = new JComboBox();

        // Add listener for change of image file selection.
        comboFileChooser.addItemListener(event ->
        {
            currentFile = (String)comboFileChooser.getSelectedItem();
            renderDetection();
        });

        // Directory for file chooser
        JTextField txtImageDir = new JTextField(currentDir, 25);
        txtImageDir.setEnabled(false);
        JButton btnImageDir = new JButton("...");

        // Add listener for new image file location button.
        btnImageDir.addActionListener(event ->
        {
            // Create a file chooser so user can select the directory
            // to find the images.
            JFileChooser fileChooser = new JFileChooser(currentDir);

            // Ensure only directories can be selected.
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

            // Show the file chooser.
            int fileChosen = fileChooser.showOpenDialog(appFrame);

            // If directory has been selected and accepted, replace the
            // contents of the file combo box with the list of image files
            // in the chosen directory.
            if (fileChosen == 0)
            {
                // Get the directory chosen by the user.
                currentDir = fileChooser.getSelectedFile().getAbsolutePath();
                txtImageDir.setText(currentDir);

                // Get the image files in the chosen directory.
                Collection<File> files = FileUtils.listFiles(new File(currentDir),
                                                    IMAGE_EXTENSIONS, false);

                // Sort the list of files in the chosen directory
                java.util.List<File> fileList = new ArrayList<>(files);
                Collections.sort(fileList);
                comboFileChooser.removeAllItems();

                // Populate the image file combo box
                fileList.forEach(file -> comboFileChooser.addItem(file.getName()));

                // Choose the first item as the current file
                if (!fileList.isEmpty())
                {
                    currentFile = (String)comboFileChooser.getSelectedItem();

                    // Update the detection and render.
                    renderDetection();
                }
            }
        });

        // Add the setup components to the top panel.
        topPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        topPanel.add(lblUseOriginalImage);
        topPanel.add(cbUseOriginalImage);
        topPanel.add(txtImageDir);
        topPanel.add(btnImageDir);
        topPanel.add(comboFileChooser);

        appFrame.add(topPanel, BorderLayout.NORTH);
    }

    /**
     * This method is called when parameters of the object
     * detection change and new detection and rendering
     * is to take place.
     */
    private void renderDetection()
    {
        // Ensure nothing happens if no image file is chosen.
        if (StringUtils.isEmpty(currentFile))
        {
            return;
        }

        // Process image for object detection based on the
        // chosen parameters.
        detector.render(currentDir + File.separator + currentFile,
                    thresholdValue, showOriginalImage, invertThresholdImage);

        // Repaint the render canvas.
        imagePanel.repaint();
    }

    /**
     * Main method of the application.
     *
     * @param args  No arguments are processed at this time.
     */
    public static void main(String[] args)
    {
        LOGGER.info("Wildlife object detector v0.1.");
        new BasicDetectorGUI();
    }
}